# Oniro WG Incubation stage marketing 2021wk50

* Chair: Agustin
* Scriba: Agustin
* Schedule: 2021-12-15

## Participants

(add/erase participants)

Shanda (EF), Agustin B.B.(EF), Gael (EF), Adrian (Huawei), Chiara (Huawei), Patrick O. (NOI), Dongni H. (Huawei), , Andrea G. (Linaro), Davide R. (huawei), Andrea B. (Synesthesia), Aurore P. (Huawei), Yves (EF) 

## Agenda

* Events list - Chiara 15 min
* Demonstrator vs Blueprint - Agustin 5 min
* Demonstrators: definition and roadmap. - Adrian 5 min
* AOB - 5 min

## MoM

### Events list

Presentation

* Chiara shows the spreadsheet where the events are being collected. #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/blob/main/Events-oniro/Oniro_events_list.ods 
* The process is similar to the one done by Huawei the previous year.
* Chiara described the events plan as a part of the story telling.

Discussion

* MWC 2022 highlights. Huawei will provide a meeting room for EF and others to have meetings about Oniro: business development.
* Seco has a big show at Embedded World and request support from Oniro. Alessandra will add this information into the events list.
* Request from Agustin to Chiara to provide a summary of the Events list effort through the mailing list explaining the rationale. Chiara suggest to have a summary on the wiki and pointing at it. #agreement 

### Demonstrator vs Blueprint

Presentation

* Check the mail sent by Agustin to the oniro-wg describing the proposal #link https://www.eclipse.org/lists/oniro-wg/msg00064.html
* Agustoin ask others to read the proposal and comment on the mailing list.

Discussion

* Let's make sure that the demonstrator follows the vision of the project.
* After having the vision we would like to reflect it in a video. Yves mention a the relevance of such videos and a couple of examples of videos where the vision is explained that had high impact.

### Demonstrators: definition and roadmap

No time for this topic. We will cover it in the next meeting.

### AOB

* Andrea Basso reminds his offering of driving a desgn thinking session. Agustin suggest to him to send a mail to the mailing list about it and he will help Andrea with the scheduling of a dedicated session.

## Relevant links

* Repo: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/tree/main/
* Actions workflow board: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/880
* Actions priority board: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/892 
* Other meetings: #link https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Meetings 
